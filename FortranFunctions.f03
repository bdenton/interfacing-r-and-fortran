

subroutine hello()
	print *, 'hello'
end subroutine hello



subroutine add( x1, x2, sum )
	 DOUBLE PRECISION, intent(in) ::  x1, x2
	 DOUBLE PRECISION, intent(inout) :: sum
	 sum = x1 + x2
end subroutine add
